# Tronity2MQTT
## Introduction:
This addon for home assistants provides the data from the website `tronity.io` for your home assistant. I made this addon for a friend, so the support a bit of limited for this addon.\
The addon is written in `go` and the sourcecode is provided since the dockerfile is compiling the app at runtime. 
___
## Functions:
With this addon you can get the \
`location`,\
`battery`,\
`odometer` and\
`range`\
of your car. And push this data to your own MQTT Server. At this point there is no TLS support.
___
## Config:
The configuration has some usable values. Every value needs to be set:

### Example:

#### Option: `id`
This option is the client ID from Tronity. 
#### Option: `secret`
This option is the generated secret from Tronity. To get the secret click in the dashboard on the symbol that looks a bit like a refresh symbol. Insert it here.
#### Option: `ip`
This is the IP Address of your mqtt server, which should receive the data.
#### Option: `port`
This is the port of your mqtt server. Default is 1883.
#### Option: `carID`
You can find the `carID` in the Tronity Dashboard. It's the unique identifier of your car.
#### Option: `mqttPrefix`
The mqtt Topics are build from two parts. for example if you wish to have `car/battery`, then this part is the `car`. Insert whatever you like but don't insert the slash. It will automatically added.
#### Option: `mqttUsername`
In home assistant this is usually your login name if you don't have a custom mqtt installation. 
#### Option: `mqttPassword`
This is usually the password. Either from home assistant or your custom mqtt installation.
#### Option: `refresh`
This are the loop in seconds. It default value is `600`. 600 seconds are 10 Minutes. So every 10 Minutes it again getting new data from the server and pushing it to you mqtt server.
#### Option: `apiurl`
This option is implemented for the safety of the app. Maybe in the future Tronity is changing their API and if this happens, hopefully changing the URL will be enough to address this problem.\
Default value for this option is\
`https://api.eu.tronity.io`\
and should be good at it is as long there are no changes to the API.
#### Option: `topics`
Everything in the `topics` area is the second part of the mqtt topic. There are default values given.\
Example:\
If your mqttPrefix is `car` and your `battery` in this part is `battery` as value, your mqtt topic to subscribe would be `car/battery`.

If you don't want to use some of the features, let the field empty in the topic tab.
For exmaple write:\
`range: ''`\
to disable the `range` push to mqtt.
___
## Issues:
### Timestamp
The timestamp is not working at this time. You can deactivate it.
___
### Location
The location data will be pushed in the format `[latitude, longitude]`.\
It's the format I needed. I hope you can also work with this format.\
If you have issues please open a issue in the repository of this addon. I can't guarantee some further activity in this project but who knows.

___
Greetings are going to my friend Diego for whom I made this addon.
--