## 2.55
##### 21.03.2021
Changed build environment
## 2.54
##### 28.12.2020
Added Icon
## 2.53
##### 28.12.2020
Fixed Odometer Bug
## 2.52
##### 28.12.2020
Small changes in config.json\
Added README.md
## 2.51 
##### 28.12.2020
Fixed rounding for odometer\
Geolocation is now in new format
## 2.50
##### 28.12.2020
App is ready to use
