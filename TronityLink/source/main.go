package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

//AuthData is data to access the API
type AuthData struct {
	ID          string `json:"id"`
	AccessToken string `json:"access_token"`
	ExpiresIn   int    `json:"expires_in"`
	TokenType   string `json:"token_type"`
}

//CarData are car data
type CarData struct {
	Level     int     `json:"level"`
	Odometer  float64 `json:"odometer"`
	Range     int     `json:"range"`
	Charging  string  `json:"charging"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Timestamp int     `json:"timestamp"`
}

//Config is the struct for the config.json
type Config struct {
	ID           string `json:"id"`
	Secret       string `json:"secret"`
	IP           string `json:"ip"`
	Port         int    `json:"port"`
	CarID        string `json:"carID"`
	MqttPrefix   string `json:"mqttPrefix"`
	MqttUsername string `json:"mqttUsername"`
	MqttPassword string `json:"mqttPassword"`
	Refresh      int    `json:"refresh"`
	Apiurl       string `json:"apiurl"`
	Topics       struct {
		Battery   string `json:"battery"`
		Range     string `json:"range"`
		Odometer  string `json:"odometer"`
		Charging  string `json:"charging"`
		Timestamp string `json:"timestamp"`
		Location  string `json:"location"`
	} `json:"topics"`
}

func main() {
	//Debug comment
	/*cmd := exec.Command("cat", "/data/options.json")
	stdout, _ := cmd.Output()
	println(string(stdout))
	*/
	file, errs := ioutil.ReadFile("/data/options.json")
	if errs != nil {
		log.Fatal(errs)
	}
	jsonData := Config{}
	errs = json.Unmarshal([]byte(file), &jsonData)
	if errs != nil {
		log.Fatal(errs)
	}

	for {
		go startup(jsonData)
		if jsonData.Refresh == 0 {
			panic("Error! Refresh time is 0! Please check config...")
		}
		time.Sleep(time.Duration(jsonData.Refresh) * time.Second)
	}

}

func startup(jsonData Config) {

	id := jsonData.ID         // "Client ID der Tronity API.")
	secret := jsonData.Secret //"Secret der Tronity API")
	ip := jsonData.IP         //"IP Adresse des MQTT Servers.")
	port := jsonData.Port
	carID := jsonData.CarID //"Dies ist die CarID die in Tronity angezeigt wird.")
	mqttUsername := jsonData.MqttUsername
	mqttPassword := jsonData.MqttPassword

	println("Starting Auth...")

	requestbody, err := json.Marshal(map[string]string{
		"client_id":     id,
		"client_secret": secret,
		"grant_type":    "app",
	})
	if err != nil {
		log.Fatal(err)
	}
	resp, err := http.Post(jsonData.Apiurl+"/oauth/authentication", "application/json", bytes.NewBuffer(requestbody))
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	var authresp AuthData
	err = json.Unmarshal(body, &authresp)
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println(authresp)

	// Bulk Get
	bulkurl := jsonData.Apiurl + "/v1/vehicles/" + carID + "/bulk"
	bearer := "Bearer " + authresp.AccessToken
	req, err := http.NewRequest("GET", bulkurl, nil)
	req.Header.Add("Authorization", bearer)
	client := &http.Client{}
	bulkresp, err := client.Do(req)
	bulkbody, _ := ioutil.ReadAll(bulkresp.Body)
	println("Rawdata: ")
	println(string(bulkbody))
	var carData CarData
	err = json.Unmarshal(bulkbody, &carData)

	//mqtt
	println("Starting mqtt server connection...")
	mqttOpts := mqtt.NewClientOptions()
	mqttOpts.AddBroker("tcp://" + ip + ":" + strconv.Itoa(port))
	mqttOpts.SetClientID("mqtt_client")
	mqttOpts.SetUsername(mqttUsername)
	mqttOpts.SetPassword(mqttPassword)
	mqttclient := mqtt.NewClient(mqttOpts)
	token := mqttclient.Connect()
	defer log.Println("Disconnecting from broker. Until next push :D")
	defer mqttclient.Disconnect(0)
	token.Wait()
	if token.Error() != nil {
		log.Fatal(token.Error().Error())
	}
	//println(token)
	var wg sync.WaitGroup

	if jsonData.Topics.Battery != "" {
		wg.Add(1)
		go publish(mqttclient, jsonData, strconv.Itoa(carData.Level), jsonData.Topics.Battery, &wg)
	}
	if jsonData.Topics.Range != "" {
		wg.Add(1)
		go publish(mqttclient, jsonData, strconv.Itoa(carData.Range), jsonData.Topics.Range, &wg)
	}
	if jsonData.Topics.Odometer != "" {
		wg.Add(1)
		go publish(mqttclient, jsonData, strconv.FormatFloat(carData.Odometer, 'f', 1, 64), jsonData.Topics.Odometer, &wg)
	}
	if jsonData.Topics.Charging != "" {
		wg.Add(1)
		go publish(mqttclient, jsonData, carData.Charging, jsonData.Topics.Charging, &wg)
	}
	if jsonData.Topics.Timestamp != "" {
		wg.Add(1)
		go publish(mqttclient, jsonData, strconv.Itoa(carData.Timestamp), jsonData.Topics.Timestamp, &wg)
	}
	if jsonData.Topics.Location != "" {
		wg.Add(1)
		gps := "["+strconv.FormatFloat(carData.Latitude, 'f', 6, 64)+","+strconv.FormatFloat(carData.Longitude, 'f', 6, 64)+"]"
		go publish(mqttclient, jsonData, string(gps), jsonData.Topics.Location, &wg)
	}

	wg.Wait()
	fmt.Printf("Push ended. Next push in about %s seconds.\n", strconv.Itoa(jsonData.Refresh))
}

func publish(client mqtt.Client, config Config, data string, topic string, wg *sync.WaitGroup) {
	defer wg.Done()
	//randnr := rand.Intn(100 - 0) + 0
	//println(randnr)
	token := client.Publish(config.MqttPrefix+"/"+topic, 0, false, data)
	token.Wait()
	if token.Error() != nil {
		log.Fatal(token.Error().Error())
	}
	fmt.Printf("Push to %s/%s was successfull.\n", config.MqttPrefix, topic)
}
